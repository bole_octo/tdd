
class Livre :
    def __init__(self, id):
        self.__id = id

    def get_id(self):
        return self.__id 

class Panier:
    def __init__(self):
        self.__dict_livres = {}
        self.__liste_coffret = []
        
    def add_livre(self, id_livre):
        if id_livre in self.__dict_livres.keys() :
            self.__dict_livres[id_livre] += 1
        else :
            self.__dict_livres[id_livre] = 1

    def get_dict_livres(self):
        return self.__dict_livres

    def calcul_coffret(self):
        copy_panier = self.__dict_livres
        while(copy_panier != {}):
            coffret_temp = []
            for key in copy_panier.copy():
                if copy_panier[key] > 1:
                    coffret_temp.append(key)
                    copy_panier[key] -= 1
                else:
                    coffret_temp.append(key)
                    del copy_panier[key]
            self.__liste_coffret.append(coffret_temp)

    def get_coffret(self):
        return self.__liste_coffret

    def calculer_prix(self):
        self.calcul_coffret()
        prix = 0
        for coffret in self.__liste_coffret:
            taille = len(coffret)
            if taille == 2:
                prix += 15.2
            elif taille == 3:
                prix += 21.6
            elif taille ==  4:
                prix += 25.6
            elif taille == 5:
                prix += 30
            else:
                prix += 8
        return prix







