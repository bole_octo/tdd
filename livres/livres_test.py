import livres as livres

def test_livres_sont_différents():
    #given
    
    #when
    livre_1 = livres.Livre(0)
    livre_2 = livres.Livre(1)

    #then 
    assert(livre_1.get_id() != livre_2.get_id())

def test_nombre_de_livres_selectionnés():
    #given
    panier = livres.Panier()
    livre_id = 0
    livre_id2 = 1
    
    #when
    panier.add_livre(livre_id)
    panier.add_livre(livre_id)
    panier.add_livre(livre_id2)

    #then
    assert(len(panier.get_dict_livres()) == 2 )
    assert(panier.get_dict_livres()[livre_id] == 2 )


def test_nombre_de_coffret_min():
    # given
    liste_livre_a_acheter = [1, 2, 0, 1, 4, 2]
    panier = livres.Panier()

    #then
    for livre_id in liste_livre_a_acheter :
        panier.add_livre(livre_id)
    panier.calcul_coffret()

    coffret = panier.get_coffret()

    assert(len(coffret) == 2)
    assert (len(coffret[0]) == 4)
    assert (len(coffret[1]) == 2)
    assert (coffret[1] == [1, 2])

def test_verification_reduction():
    liste_livre_a_acheter = [1, 2, 0, 1, 4, 2]
    panier = livres.Panier()

    # then
    for livre_id in liste_livre_a_acheter:
        panier.add_livre(livre_id)
    price = panier.calculer_prix()
    assert (price == 40.8)

# verifier que deux livres sont différents
# calcul le nombre le nombre de chaque copie du livre
# calcul du nombre de coffret minimum
# verifier que la reduction soit appliquée en fonction du bon nombre de coffret
#verifier somme totale panier

