import src_tennis as tennis
from const import point, player_0, player_1

def test_initiate_game():
    #given
    #When
    list_point: list[list] = tennis.initiate_game()
    # Then
    assert (len(list_point) == 2)

    # Sous tableaux des sets
    assert(list_point[0][0] == 0)
    assert(list_point[0][1] == 0)
    assert (len(list_point[0]) == 2)

    # Sous tableau des points
    assert(list_point[1][0] == point[0])
    assert(list_point[1][1] == point[0])
    assert (len(list_point[1]) == 2)
    


def test_first_player_win_first_point():
    """
    Le premier joueur gagne le premier point donc le tableau renvoie ['15', 'love']
    """
    # given
    expected_point_list = [point[1],point[0]]
    player = 0
    tab_game = tennis.initiate_game()
    # When
    list_point: list[list] = tennis.win_point(player, tab_game)
    # Then
    assert (list_point[1][0] == expected_point_list[0])
    assert (list_point[1][1] == expected_point_list[1])

def test_second_player_win_two_first_points():
    """
      Le deuxième joueur gagne les deux premiers points donc le tableau renvoie ['love','30']
    """
    # given
    expected_point_list = [point[0],point[2]]
    player = 1

    tab_game = [[0,0],[point[0], point[1]]]
    # When
    list_point: list[list] = tennis.win_point(player, tab_game)
    # Then
    assert (list_point[1][0] == expected_point_list[0])
    assert (list_point[1][1] == expected_point_list[1])


def test_second_player_from_30_to_deuce():
    """
    Le deuxième joueur égalise à 40 le tableau renvoie ['deuce','deuce']
    """
    # given
    expected_point_list = [point[4], point[4]]
    player = 1
    tab_game = [[0,0],[point[3], point[2]]]
    # When
    list_point: list[list] = tennis.win_point(player, tab_game)
    # Then
    assert (list_point[1][0] == expected_point_list[0])
    assert (list_point[1][1] == expected_point_list[1])

def test_second_player_adv_from_deuce():
    """
    Le deuxième joueur passe à adv le tableau renvoie ['deuce','adv']
    """
    # given
    expected_point_list = [point[4], point[5]]
    player = 1
    tab_game = [[0,0],[point[4], point[4]]]
    # When
    list_point: list[list] = tennis.win_point(player, tab_game)
    # Then
    assert (list_point[1][0] == expected_point_list[0])
    assert (list_point[1][1] == expected_point_list[1])

def test_second_player_win_point_deuce_from_adv():
    """
    Le deuxième joueur passe à deuce le tableau renvoie ['deuce','deuce'] (retour en arrière)
    """
    # given
    expected_point_list = [point[4], point[4]]
    player = 1
    tab_game = [[0,0],[point[5], point[4]]]
    # When
    list_point: list[list] = tennis.win_point(player, tab_game)
    # Then
    assert (list_point[1][0] == expected_point_list[0])
    assert (list_point[1][1] == expected_point_list[1])

def test_first_player_win_with_0_40():
    # given
    expected_tab = [[1,0],[point[0], point[0]]]
    player = 0
    tab_game = [[0,0],[point[3], point[0]]]
    # When
    new_tab_game : list[list] = tennis.win_point(player,tab_game)
    # Then
    assert (new_tab_game[0][0] == expected_tab[0][0])
    assert (new_tab_game[0][1] == expected_tab[0][1])
    assert (new_tab_game[1][0] == expected_tab[1][0])
    assert (new_tab_game[1][1] == expected_tab[1][1])

def test_first_player_win_with_adv_deuce():
    # given
    expected_tab = [[1,0],[point[0], point[0]]]
    player = 0
    tab_game = [[0,0],[point[5], point[4]]]
    # When
    new_tab_game : list[list] = tennis.win_point(player,tab_game)
    # Then
    assert (new_tab_game[0][0] == expected_tab[0][0])
    assert (new_tab_game[0][1] == expected_tab[0][1])
    assert (new_tab_game[1][0] == expected_tab[1][0])
    assert (new_tab_game[1][1] == expected_tab[1][1])

def test_fisrt_player_win_the_game_with_4_1_sets():
    # given
    expected_message : str = 'Player 0 wins the game'
    player = 0
    tab_game = [[3, 1], [point[3], point[0]]]
    # When
    message : str = tennis.win_point(player, tab_game)
    # Then
    assert (str(message) == str(expected_message))

def test_fisrt_player_ties_the_game_with_3_3_sets():
    # given
    expected_tab = [[3, 3], [point[0], point[0]]]
    player = 1
    tab_game = [[3, 2], [point[0], point[3]]]
    # When
    new_tab_game : list[list] = tennis.win_point(player, tab_game)
    # Then
    assert (new_tab_game[0][0] == expected_tab[0][0])
    assert (new_tab_game[0][1] == expected_tab[0][1])
    assert (new_tab_game[1][0] == expected_tab[1][0])
    assert (new_tab_game[1][1] == expected_tab[1][1])



def test_printing_scores():
    """
    Le player 0 gagne un point et on vérifie l'affichage des scores
    """
    # given
    expected_message : str = 'Player 0 has 15 points and Player 1 has 30 points'
    tab_game = [[0, 0], [point[1], point[2]]]
    # When
    message : str = tennis.printing_scores(tab_game)
    # Then
    assert (str(message) == str(expected_message))

def test_match_highlights():
    """
    On affiche le déroulé du match
    """
    # given
    expected_score_sum_up = ['Player 0 has love points and Player 1 has love points',
                             'Player 0 has 15 points and Player 1 has love points',
                             'Player 0 has 30 points and Player 1 has love points',
                             'Player 0 has 30 points and Player 1 has 15 points',
                             'Player 0 has 40 points and Player 1 has 15 points',
                             'Player 0 wins the game']
    tab_game = [[0, 0], [point[0], point[0]]]
    player_win_point_list = [player_0, player_0, player_1, player_0, player_0]
    score_sum_up = [tennis.printing_scores(tab_game)]
    # When
    for player in player_win_point_list:
        tab_game = tennis.win_point(player, tab_game)
        tennis.printing_scores(tab_game)
        score_sum_up.append(tennis.printing_scores(tab_game))

    #Then
    assert(len(score_sum_up) == len(expected_score_sum_up))
    for i in range(len(score_sum_up)):
        assert (score_sum_up[i] == expected_score_sum_up[i])
