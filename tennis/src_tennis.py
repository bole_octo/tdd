from const import point, number_of_game_to_win


def initiate_game():
    return [[0, 0], [point[0], point[0]]]


def win_point(player, tab_game):
    current_player_score = tab_game[1][player]
    # Le player courant gagne son point à 30 contre 40, il égalise à 'deuce'
    if current_player_score == point[2] and tab_game[1][1-player] == point[3]:
        tab_game[1] = [point[4], point[4]]

    # Le player gagne son point à deuce contre avantage, le joueur adverse perd son avantage, il égalise à 'deuce'
    elif current_player_score == point[4] and tab_game[1][1-player] == point[5]:
        tab_game[1] = [point[4], point[4]]

    # Le player gagne son point à 40 ou à adv, il gagne le set
    elif current_player_score == point[3] or current_player_score == point[5]:
        tab_game[0][player] += 1
        tab_game[1][0] = point[0]
        tab_game[1][1] = point[0]
        return f'Player {player} wins the game'

    # Le player gagne son point sans cas spécial sur le score, il incrémente son score
    else:
        new_player_score_index = point.index(current_player_score) + 1
        tab_game[1][player] = point[new_player_score_index]

    # Verification gain du match
    if (tab_game[0][player] == number_of_game_to_win) & (tab_game[0][player-1] < number_of_game_to_win - 2):
        return f'Player {player} wins the game'
    else:
        return tab_game

def printing_scores(tab_game):
    return f'Player 0 has {tab_game[1][0]} points and Player 1 has {tab_game[1][1]} points'
