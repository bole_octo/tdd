import datetime
from operator import attrgetter
import requests

class Banque:
    def __init__(self, iban, http_client):
        self.__compte = Compte(0, iban, http_client)
    
    def actions_possibles(self):
        print()
        print("1. Ajouter de l'argent")
        print("2. Retirer de l'argent")
        print("3. Voir le solde")
        print("4. Voir l'historique des transactions")
        print("5. Quitter")

    def choix_utilisateur(self):
        print()
        return int(input("Que voulez-vous faire ? "))

    def choix_action(self, choix):
        buffer = ""
        if choix == 1:
            print("Combien voulez-vous ajouter ? ")
            argent = self.input_valeur()
            self.ajouter_argent(argent)
        elif choix == 2:
            print("Combien voulez-vous retirer ? ")
            argent = self.input_valeur()
            self.retirer_argent(argent)
        elif choix == 3:
            buffer = self.voir_le_solde()
        elif choix == 4:
            self.voir_historique_des_transactions()
        elif choix == 5:
            self.quitter()
        else:
            print("Choix invalide")
        return buffer
    
    def input_valeur(self):
        argent = float(input())
        return argent

    def ajouter_argent(self, argent):
        validation_ajout_argent = self.__compte.ajout_argent(argent)
        if validation_ajout_argent:
            print("Ajout d'argent effectué")
        else:
            print("Ajout d'argent non effectué")
        self.voir_le_solde()
    
    def retirer_argent(self, argent):
        validation_retrait_argent = self.__compte.retire_argent(argent)
        if validation_retrait_argent:
            print("Retrait d'argent effectué")
        else:
            print("Retrait d'argent non effectué")
        self.voir_le_solde()
    
    def voir_le_solde(self):
        buffer = f"Votre solde est de {self.__compte.voir_solde()} €"
        print(buffer)
        return buffer
    
    def voir_historique_des_transactions(self):
        historique = self.__compte.get_historique_transaction()
        if len(historique) == 0:
            print("Vous n'avez pas encore effectué de transactions")
        for transaction in historique:
            print(transaction.afficher())
    
    def quitter(self):
        print("Merci pour tes sous :D !")
        exit()

class Compte :
    def __init__(self, id, iban, http_client):
        self.__id = id
        self.__solde = 0
        self.__historique = []
        self.__iban = iban 
        self.__http_client = http_client

    def ajout_argent(self, argent: float):
        if argent < 0:
            print("Ne nous arnaque pas, entre une valeur positive")
            return False
        self.__solde = self.__solde + argent
        self.__historique.append(Transaction(datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S.%f"), f'+{argent}', self.__solde))
        return True

    def retire_argent(self, argent: float) -> bool:
        if argent < 0:
            print("Ne nous arnaque pas, entre une valeur positive")
            return False
        if self.__solde >= argent:
            self.__solde = self.__solde - argent
            self.__historique.append(Transaction(datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S.%f"), f'-{argent}', self.__solde))
            return True
        print("Vous n'avez pas assez d'argent")
        return False

    def voir_solde(self):
        return self.__solde

    def get_historique_transaction(self):
        self.__historique.sort(key=lambda x: str(x.date), reverse=True)
        return self.__historique

    def set_historique(self, historique):
        self.__historique = historique
    
    def get_iban(self):
        return self.__iban

    def transferer_argent_par_iban(self, iban_to, somme):
       result = self.retire_argent(somme)
       if result:
              # Connexion à l'API et envoie de la requête
              return self.__http_client.fetch("https://api.com/transfer",{"ibanFrom": self.__iban, "ibanTo": iban_to, "Amount": somme})
       return result
       


class Transaction :
    def __init__(self, date, operation, solde):
        self.date = date
        self.__operation = operation
        self.__solde = solde

    def afficher(self):
        return f'{self.date} | {self.__operation} | {self.__solde}'

    def get_solde(self):
        return self.__solde

    def set_date(self, date):
        self.date = date

    def get_date(self):
        return self.date
    
# Mock API connexion
class HTTP_client:
    def __init__(self):
        pass
    
    def fetch(self, url, body=dict):
        response = requests.post(url,body)
        return response.status_code