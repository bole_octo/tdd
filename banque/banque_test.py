from unittest.mock import Mock
import requests
import pytest

import banque as banque
import time
import datetime


class HTTP_client_mock:
    def __init__(self):
        pass

    def fetch(self, url, body):
        return 202


#variables globales
iban = "FR123456789"
client_mock = HTTP_client_mock()


# Voir le solde
# Aujouter de l'argent
# Retirer de l'argent

date_format = '%Y-%m-%d'

def test_voir_solde():
    # given
    # when
    compte = banque.Compte(0, iban, client_mock)
    solde = compte.voir_solde()
    # then
    assert(solde == 0)


def test_ajouter_argent():
    # given
    argent_a_ajouter = 40
    # when
    compte = banque.Compte(0, iban, client_mock)
    compte.ajout_argent(argent_a_ajouter)
    solde = compte.voir_solde()
    # then
    assert(solde == argent_a_ajouter)


def test_retirer_argent_si_solde_suffisant():
    # given
    solde_inital = 3500
    argent_a_retirer = 500
    expected_solde = 3000
    # when
    compte = banque.Compte(0, iban, client_mock)
    # Ceci parce que on a pas initialiser compte avant avec autre valeur
    compte.ajout_argent(solde_inital)
    transact = compte.retire_argent(argent_a_retirer)
    solde = compte.voir_solde()
    # then
    assert(solde == expected_solde)
    assert transact


def test_retirer_argent_si_solde_insuffisant():
    # given
    solde_inital = 50
    argent_a_retirer = 500
    # when
    compte = banque.Compte(0, iban, client_mock)
    # Ceci parce que on a pas initialiser compte avant avec autre valeur
    compte.ajout_argent(solde_inital)
    transact: bool = compte.retire_argent(argent_a_retirer)
    solde = compte.voir_solde()
    # then
    assert(solde == solde_inital)
    assert(transact == False)

def test_verification_non_argent_positif_sur_ajout():
    #given
    solde_ajouter = - 50
    solde_initial = 10
    #when
    compte = banque.Compte(0, iban, client_mock)
    # Ceci parce que on a pas initialiser compte avant avec autre valeur
    compte.ajout_argent(solde_initial)
    transact_retire: bool = compte.ajout_argent(solde_ajouter)

    solde = compte.voir_solde()
    # then
    assert (solde == solde_initial)
    assert (transact_retire == False)

def test_verification_non_argent_positif_sur_retire():
    # given
    solde_a_retirer = -50
    solde_initial = 10
    # when
    compte = banque.Compte(0, iban, client_mock)
    # Ceci parce que on a pas initialiser compte avant avec autre valeur
    compte.ajout_argent(solde_initial)
    transact_retire: bool = compte.retire_argent(solde_a_retirer)

    solde = compte.voir_solde()
    # then
    assert (solde == solde_initial)
    assert (transact_retire == False)

def test_afficher_transaction():
    # given
    solde = 10
    date = datetime.date.today()
    operation = 10
    # when
    transac = banque.Transaction(date,operation,solde)
    transac_string = transac.afficher()
    # then
    assert(transac_string == f'{date} | {operation} | {solde}')

def test_ajout_argent_creer_historique_de_taille_2():
    # given
    historique =[]
    ajout_argent_1 = 100
    ajout_argent_2 = 50
    date = datetime.date.today()
    #when
    compte = banque.Compte(0, iban, client_mock)
    compte.ajout_argent(ajout_argent_1)
    compte.ajout_argent(ajout_argent_2)
    historique = compte.get_historique_transaction()

    #then
    assert(len(historique)==2)
    assert(historique[0].get_solde()==100)
    assert(historique[1].get_solde()==150)

def test_ajout_argent_creer_historique_de_taille_2():
    # given
    historique =[]
    ajout_argent_1 = 100
    retirer_argent_2 = 50
    date = datetime.date.today()

    #when
    compte = banque.Compte(0, iban, client_mock)
    compte.ajout_argent(ajout_argent_1)
    compte.retire_argent(retirer_argent_2)
    historique = compte.get_historique_transaction()

    #then
    assert(len(historique)==2)
    assert(historique[0].get_solde()==50)
    assert(historique[1].get_solde()==100)


def test_historique_correctement_trie_par_date():
   ajout_argent_1 = 100 #première transaction
   retirer_argent_2 = 20 #deuxièmetransaction
   ajout_argent_3 = 50 #troisième transaction

   # when
   compte = banque.Compte(0, iban, client_mock)
   compte.ajout_argent(ajout_argent_1)
   time.sleep(0.2)
   compte.retire_argent(retirer_argent_2)
   time.sleep(0.2)
   compte.ajout_argent(ajout_argent_3)
   time.sleep(0.2)
   historique = compte.get_historique_transaction()

   # then
   assert (len(historique) == 3)
   assert (historique[0].get_date() > historique[1].get_date())
   assert(historique[0].get_solde() == 130)
   assert (historique[1].get_date() > historique[2].get_date())

def test_validation_du_menu():
    # given
    choix = 3
    # when
    my_bank = banque.Banque(iban, client_mock)
    buffer = my_bank.choix_action(choix)
    # then
    assert(buffer == "Votre solde est de 0 €")

def input_argent_est_un_float():
    # given
    argent = 10.5
    # when
    my_bank = banque.Banque()
    argent = my_bank.input_valeur()
    # then
    assert (argent == 10.5)


def test_mock_transferer_argent_par_iban():
    # given
    compte_1 = banque.Compte(0, iban, client_mock)
    iban_to = "FR223456789"
    somme = 200
    # When
    compte_1.ajout_argent(somme)
    message_retour_transfert = compte_1.transferer_argent_par_iban(iban_to, somme)
    # Then
    assert(message_retour_transfert == 202)
    assert(compte_1.voir_solde() == 0)


