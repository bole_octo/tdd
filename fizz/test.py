from FizzBuzz import fizz_buzz_function

def test_return_1_if_1():
    #given
    number = 1
    #when
    result_fizz_buzz = fizz_buzz_function(number)
    #Then
    assert(result_fizz_buzz=='1')

def test_return_fizz_if_3():
    #given
    number = 3
    #when
    result_fizz_buzz = fizz_buzz_function(number)
    #Then
    assert(result_fizz_buzz=='Fizz')

def test_return_buzz_if_5():
    #given
    number = 5
    #when
    result_fizz_buzz = fizz_buzz_function(number)
    #Then
    assert(result_fizz_buzz=='Buzz')

def test_return_buzz_if_15():
    #given
    number: int = 15
    #When
    result_fizz_buzz = fizz_buzz_function(number)
    #Then
    assert(result_fizz_buzz=='FizzBuzz')

def test_return_fizz_if_multiple_3():
    #given
    number = 6
    #when
    result_fizz_buzz = fizz_buzz_function(number)
    #Then
    assert(result_fizz_buzz=='Fizz')

def test_return_fizz_if_multiple_5():
    #given
    number = 10
    #when
    result_fizz_buzz = fizz_buzz_function(number)
    #Then
    assert(result_fizz_buzz=='Buzz')

def test_return_fizz_if_multiple_3_et_5():
    #given
    number = 30
    #when
    result_fizz_buzz = fizz_buzz_function(number)
    #Then
    assert(result_fizz_buzz=='FizzBuzz')